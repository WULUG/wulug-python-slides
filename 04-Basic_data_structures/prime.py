from math import sqrt

def is_prime(n):
    if n <= 1:
        return False

    root = int(sqrt(n))

    for i in range(2, root+1):
        if n % i == 0:
            return False

    return True
