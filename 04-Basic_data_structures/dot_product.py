def dot_product(vec1, vec2):
    if len(vec1) is not len(vec2):
        raise Exception('The vectors are not the same length')

    sum_vector = []

    for a, b in zip(vec1, vec2):
        product = a * b
        sum_vector.append(product)

    return sum(sum_vector)

def main():
    vec1 = (1, 2, 3)
    vec2 = (5, 1, 1)

    matrix1 = [
        [1,2,3],
        [4,5,6],
        [7,8,9]
    ]

    for row in matrix1:
        print(row)


    # product = dot_product(vec1, vec2)

    # print(f'The dot products of {vec1} and {vec2} is {product}')

if __name__ == '__main__':
    main()
