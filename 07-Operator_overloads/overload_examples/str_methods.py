class Foo():
    def __init__(self, data = 10):
        self._data = data

    def __str__(self):
        return f'this object has {self._data}'


class Bar():
    def __init__(self, data = 20):
        self._data = data


def main():
    foo = Foo()
    bar = Bar()

    print(foo)
    print(bar)


if __name__ == '__main__':
    main()
