from functools import singledispatch #importing the single dispatch algorithm from the Python API

class Foo():
    '''Just a random class for testing purposes'''
    def __init__(self, something):
        self._data = something

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, new_data):
        self._data = new_data


@singledispatch # marking a function to be overloadable
def generic(par):
    """Defining a generic function to be 'overloaded'"""

    print(f'this is generic and the parameter was {par}')


@generic.register(int) # defining an implementation of generic for integers
def int_generic(par):
    '''Implementing for integers'''
    print(f'Printing {par} times')

    for n in range(par):
        print(f'{n} this is the generic version for integers')


@generic.register(Foo)
def generic_foo(par):
    '''Implementing for Foo Class'''

    print(f'This foo object has {par.data}')


@singledispatch
def binary_function(op1, op2):
    print(f'printing {op1} {op2}')

@binary_function.register(int)
def int_float_bin(op1, op2=(22/7)):
    print(f'{op1} is an integer')
    print(f'{op2} is an float')


def just_a_normal_function(op1, op2=(22/7)):
    for _ in range(op1):
        print(f'this prints pi {op2}')

def main():
    generic('foo')
    obj = Foo(['list'])
    generic(obj)

    binary_function(10,2.0)
    binary_function(2.0,1)
    binary_function(3) # this errors out in pylint but runs in the 3.7.4 interpreter

    just_a_normal_function(2) # However this one won't give you an error in static analysis

if __name__ == '__main__':
    main()
