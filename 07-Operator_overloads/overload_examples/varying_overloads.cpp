#include <iostream>

int sum(int op1, int op2);

// int sum(int op1, int op2, int op3);

int sum(int op1, int op2, int op3, int op4=0);

int main() {

    int a, b, c;

    a = b = c = 30;

    std::cout << "the sum is " << sum(a, b) << std::endl;
    std::cout << "the sum is " << sum(a, b, c) << std::endl;


    return 0;
}

int sum(int op1, int op2) {
    return op1 + op2;
}

// int sum(int op1, int op2, int op3) {
//     return op1 + op2 + op3;
// }

int sum(int op1, int op2, int op3, int op4) {
    return op1 + op2 + op3 + op4;
}
