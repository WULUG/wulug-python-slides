class Foo():
    '''Just a random class for testing purposes'''
    def __init__(self, something=None):
        self._data = something

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, new_data):
        self._data = new_data

    def __bool__(self):
        return self._data != None


class Bar():
    '''Just a random class for testing purposes'''
    def __init__(self, something=None):
        self._data = something

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, new_data):
        self._data = new_data


def test_obj(obj, name):
    if obj:
        print(f'{name} was not default constructed')
    else:
        print(f'{name} was default constructed')


def main():
    foo = Foo(10)
    qux = Foo()
    bar = Bar()
    baz = Bar(20)

    test_obj(foo, 'foo')
    test_obj(qux, 'qux')
    test_obj(bar, 'bar')
    test_obj(baz, 'baz')




if __name__ == '__main__':
    main()
