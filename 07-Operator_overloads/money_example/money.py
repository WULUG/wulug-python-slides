'''Example Class is an example'''


class Money():
    '''Money class to model money irl
    specifically USD'''

    # static data member used to keep track of
    # how many monies is in circulation
    circulation = 0

    def __init__(self, dollars=1, cents=0):
        '''Constructor Aka init method used to
        create an instance of the object'''
        self._dollars = dollars
        self._cents = cents
        Money.circulation += 1


    def __del__(self):
        '''Destructor decrements the circulation
        data member everytime a Money object is
        destroyed'''
        Money.circulation -= 1


    # ACCESSORS

    @property
    def dollars(self):
        '''dollars accessor using the property tag'''
        return self._dollars


    @property
    def cents(self):
        '''cents accessor using the property tag'''
        return self._cents


    @property
    def as_cents(self):
        '''returns the total value as all cents.'''
        return (self._dollars * 100) + self._cents


    @property
    def as_string(self):
        '''returns the total value as a string'''
        return self.__str__()

    # MUTATORS

    @dollars.setter
    def dollars(self, dollars):
        self._dollars = dollars


    @cents.setter
    def cents(self, cents):
        if cents > 99:
            self._dollars += int(cents/100)
            self._cents = cents % 100
        else:
            self._cents = cents

    @as_string.setter
    def as_string(self, string_val):
        try:
            dollars, cents = string_val.split('.')
        except ValueError:
            dollars, cents, *_ = string_val.split('.')

        self.dollars = dollars
        self.cents = cents


    # Overloads
    def __str__(self):
        return f'{self._dollars}.{self._cents}'

    def __repr__(self):
        return self.__str__()

    # Operators
    def __add__(self, other):
        new = Money()

        new.dollars = self.dollars + other.dollars
        new.cents = self.cents + other.cents

        return new
