'''test file for money class'''

import money

def default_constructor():
    default = money.Money()

    if default.dollars == 1 and default.cents == 0:
        return True

    return False


def constructor():
    these = money.Money(10,30)

    if these.dollars == 10 and these.cents == 30:
        return True

    return False


def test_function(name, func):
    print(f'testing {name} result {func()}')


def main():
    test_function('default constructor', default_constructor)
    test_function('constructor', constructor)

if __name__ == '__main__':
    main()
