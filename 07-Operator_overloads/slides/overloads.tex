\section{Overloads}
\begin{frame}[fragile]
  \frametitle{Overloads}

  In the previous presentation we saw a bit of code that looked like this\ldots

\begin{verbatim}
def __str__(self):
    return f'{self._dollars}
            .{self._cents}'

def __repr__(self):
    return self.__str__()
\end{verbatim}

  What these are called are overloads.

  \vspace{3mm}

  Overloading is defining more than one implementation for a function.

\end{frame}

\begin{frame}[fragile]
  \frametitle{Overloads}
  \framesubtitle{A Java example}

  In most languages overloading is a common practice and is allowed on 90\% of the
  functions present in a language or program.

\begin{verbatim}
public int add(int op1, int op2) {
    return op1 + op2;
}

public float add(float op1, float op2){
    return op1 + op2;
}
\end{verbatim}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Overloads}
  \framesubtitle{PEP-443}

  Instead of writing multiple specific functions that essentially does the same thing
  Python prefers that we write Generic functions that can accept all types
  instead.

  \vspace{3mm}

  So what we'll do is instead write a generic function that is comprised of multiple
  other functions and use a specific algorithm, called a dispatch algorithm.

\end{frame}

\begin{frame}[fragile]
  \frametitle{Overloads}
  \framesubtitle{Single Dispatch}

  Python implements the single dispatch algorithm within the functools module.

  \vspace{3mm}

  So you can define a generic function using the
  @singledispatch decorator.\footnotemark{}


\begin{verbatim}
from functools import singledispatch

@singledispatch
def generic(par):
    print(f`this is generic
          and the parameter was {par}')

\end{verbatim}

  \footnotetext[1]{examples pulled from the
    \href{https://www.python.org/dev/peps/pep-0443/}{PEP-443 Document}}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Overloads}
  \framesubtitle{Implementing overloads}

  The function \verb|generic(par)| is now defined as a local property

  \vspace{3mm}

  To add overload implementations you'll decorate the next functions with the
  register(type) attribute of the function property.

\begin{verbatim}
@generic.register(int)
def int_generic(par):
    print(f`Printing {par} times')

    for n in range(par):
        print(f`{n} this is generic')
\end{verbatim}

\end{frame}

\subsection{Pitfall 1}
\begin{frame}[fragile]
  \frametitle{Pitfall 1}

  Naming our specific implementations of a generic functions the same name
  as the original generic will override the generic version.

  \vspace{3mm}

  This can be a useful mechanic, but should generally be avoided.
\end{frame}

\begin{frame}[fragile]
  \frametitle{Pitfall Output}
\begin{verbatim}
Printing foo times
Traceback (most recent call last):
  File "basic_overloading.py",
line 19, in <module>
    generic(`foo')
  File "basic_overloading.py",
line 15, in generic
    for n in range(par):
TypeError: `str' object cannot be
interpreted as an integer

\end{verbatim}
\end{frame}

\subsection{Pitfall 2}
\begin{frame}[fragile]
  \frametitle{Pitfall 2}
  So then what about multiple types?
  \pause{}
  No
  \pause{}\\

  \vspace{3mm}

  The Singledispatch algorithm can only handle one type at a time.

\begin{verbatim}
@singledispatch
def binary_function(op1, op2):
    print(f`printing {op1} {op2}')
\end{verbatim}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Pitfall 2}

  But that won't stop us from defining it for a function with
  multiple arguments anyways
\begin{verbatim}
@binary_function.register(int)
def int_float_bin(op1, op2):
    print(f`{op1} is an integer')
    print(f`{op2} is an float')
\end{verbatim}

  But keep in mind that the singledispatch decorator will only look at the first argument.

\end{frame}

\subsection{Varying number of Parameters}
\begin{frame}[fragile]
  \frametitle{Overloads}

  The single dispatch algorithm is nice, but it still doesn't cover the case for having
  varying amounts of parameters.

\begin{verbatim}
int sum(int op1, int op2) {
    return op1 + op2;
}

int sum(int op1, int op2, int op3) {
    return op1 + op2 + op3;
}
\end{verbatim}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Overloads}
  But if we look closer this set of functions at max only had three parameters.

\begin{verbatim}
int sum(int op1, int op2) {
    return op1 + op2;
}

int sum(int op1, int op2, int op3) {
    return op1 + op2 + op3;
}
\end{verbatim}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Overloads}
  Which means we could reduce the two functions down to one.

\begin{verbatim}
int sum(int op1, int op2, int op3 = 0);

int sum(int op1, int op2, int op3) {
    return op1 + op2 + op3;
}
\end{verbatim}

And what these are called are default parameters.\\
\tiny{There is also another solution but we'll get there when
we're ready.}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Default Values}

  You can set default values similarly to how you would in C++.

\begin{verbatim}
@binary_function.register(int)
def int_float_bin(op1, op2=(22/7)):
    print(f`{op1} is an integer')
    print(f`{op2} is an float')
\end{verbatim}

  Just to let you guys know\ldots

\begin{verbatim}
binary_function(3)
\end{verbatim}

  Will run in the python interpreter, but the static analysis tools will
  see this as an error, so use this sparingly.

\end{frame}
