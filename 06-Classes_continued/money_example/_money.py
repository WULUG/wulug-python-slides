'''test file for money class'''
from money import *


def test_properties(fast=True):
    '''tests accessor functionality'''

    expected_values = (10, 20, 1020, '10.20', 1)

    monopoly = Money(10, 20)

    dollars = monopoly.dollars
    cents = monopoly.cents
    only_cents = monopoly.as_cents
    bill = monopoly.as_string
    circulating = Money.circulation

    results = (dollars, cents, only_cents, bill, circulating)

    if fast:
        return results == expected_values


    print('testing properties iteratively')
    for expectation, result in zip(expected_values, results):
        print(f'Expectation: {expectation}')
        print(f'     Result: {result}')

        assert (expectation == result), 'This was not what I expected'

    return True


def main():
    ''' main part of where tests will be ran'''
    print('testing properties {}'.format(test_properties(False)))


if __name__ == '__main__':
    main()
