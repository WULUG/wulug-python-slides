def factorial(n):
    if n in (0, 1):
        return 1
    else:
        return n*factorial(n-1)


def looptorial(n):
    if n in (0, 1):
        return 1
    else:
        k = 1
        for x in range(2, n+1):
            k = k * x
        return k


def main():
    k = 4
    n = factorial(k)
    b = looptorial(k)
    print(f'{k} factorial {n}')
    print(f'{k} factorial {b}')

if __name__ == '__main__':
    main()
