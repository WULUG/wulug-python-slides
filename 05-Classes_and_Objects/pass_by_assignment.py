def foo(n):
    n = 10


def bar(l):
    l[0] = 20


def main():
    a = 45
    b = [102]

    print(f'variable a is {a}')

    foo(a)

    print(f'variable a is {a}')


    print(f'variable b[0] is {b[0]}')

    bar(b)

    print(f'variable b[0] is {b[0]}')


if __name__ == '__main__':
    main()
